# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: Ahmad Inayat
#    Skype: ahmad.inayat4
#    Phone:+923349275408
#
##############################################################################
{
    'name': 'Access Restriction By MAC Address',
    'summary': """User Can Access His Account Only From Specified MAC Address""",
    'version': '13.0.1.0.0',
    'description': """User Can Access His Account Only From Specified MAC Address""",
    'author': 'Ahmad Inayat',
    'company': 'Ahmad Inayat',
    'website': 'https://www.ahmad.com',
    'category': 'Tools',
    'depends': ['base', 'mail'],
    'license': 'AGPL-3',
    'data': [
        'security/ir.model.access.csv',
        'views/allowed_mac_view.xml',
    ],
    'images': ['static/description/banner.png'],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'price' : 15,
    'currency': 'USD'
}

