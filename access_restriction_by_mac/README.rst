Access Restriction By MAC Address
============================

This module will restrict users access to his account from allowed MAC Address only. If user access his
account from  non-allowed MAC Address, login will be restricted and a warning message will be displayed in
login page.

If no MAC address is specified for a user, then there will not be restriction by MAC Address. He can access from any Address.


Credits
=======
Ahmad Inayat

Author
------
* Ahmad Inayat <ahmadinayat75@gmail.com>
